Artusi
============

<<<<<<< HEAD
The documentation of Artusi is written in SSOUP BI Document format using JSON-LD serialization.
It can be viewed through Artusi itself at <a href="http://alebellu.github.com/artusi-ssoup/docs/">this page</a>.

Artusi is a Javascript implementation of <a href="https://github.com/alebellu/ssoup">SSOUP</a> that can run in a web browser.

<a href="http://www.pellegrinoartusi.it/en/pellegrino-artusi/">
<img width="150" src="http://upload.wikimedia.org/wikipedia/it/a/a3/Pellegrino_Artusi.jpg" />
</a>
<br />
<a href="http://www.pellegrinoartusi.it/en/pellegrino-artusi/">Pellegrino Artusi</a> was the author of
“the kitchen science and the art of well eating” a real boundary line in the gastronomic culture of the XIX century.
http://www.justfoodnow.com/2011/09/21/the-father-of-italian-food-pellegrino-artusi/
Artusi was a scientist & gastronomist at heart & each recipe in the book is the end result of many tests and trials. He loved food, loved to eat and was a perfectionist both when cooking and when writing.

It is currently based on <a href="http://jquery.com/">JQuery</a> and <a href="http://json-ld.org/">JSON-LD</a> libraries.

See <a href="https://github.com/alebellu/artusi-ssoup/tree/gh-pages">gh-pages</a> branch for the code.

#Services
Services are indexed in Artusi SSOUP through an index file named service.jsonld, which should reside in the home folder of the registry.

#TODO
- click su pulsante edit lancia il metodo editMode del main visualization pan
  - il metodo editMode sostituisce il pusante Edit in alto a destra con i pulsanti Save, Cancel e Done
  - il metodo editMode registra l'handler hover su tutti i div che sono passati dal metodo view
    - l'handler hover mostra sopra il div a sinistra un'icona a forma di penna (per edit) ed una icona a forma di
      ingranaggio (o di ricetta) per configurare i servizi, e un pannellino di supporto sulla sinistra del div che consente di farne il drag and drop
      - un click sull'icona a forma di penna fa si che venga richiamato il metodo edit per il div passando dalla visualizzazione all'editing
      - un click sull'icona a forma di ingranaggio apre un popup per la configurazione dei servizi

- un click sul pulsante Save in edit mode salva i dati modificati nei rispettivi drawers di provenienza e salva
  inoltre i dati di configurazione dei servizi nello stesso drawer del dato. Perche' questo sia possibile e' necessario
  che vengano incluse nei dati le informazioni sulla provenance. Inoltre a fianco dei dati dovranno essere salvate le
  informazioni che riguardano il delta,ovvero le modifiche, inserimenti e cancellazioni effettuate sul dato.
  - la drawer.get dovra'ritornare (da specifiche ssoup!!) insieme al dato anche delle informazioni sulla provenance: in artusi
    in ogni proprieta' ci sara' la sottoproprieta' "sw:provenance" con all'interno le info di provenance per le altre sottoproprieta'.
   Se i llivello base gia' non lo contiene dovra'esser messa come location di provenance il drawer stesso!
  - la drawer.put potra'leggere le informazioni sui dati modificati nella risorsa (in ogni proprieta' ci sara' "sw:modifications")
    per decidere quali istruzioni eseguire nel drawer ed inoltre (da specifiche ssoup!!) dovra'trattare adeguatamente e salvare le info di provenance passatealla put insieme al dato.
  - il kitchen.assistant.save leggera' la risorsa livello per livello ed inviera' ad ogni drawer le informazioni di
    sua competenza leggendo i dati di provenance, ed evitera' di inviare i messaggi ai drawer che non hanno modifiche ai dati.

- tra gli editor del tipo rdfs:Container ci saranno dei gestori di layout, come AbsoluteLayoutEditor,
  FlowLayoutEditor, ecc., per cui se si vuole che un'istanza possa essere visualizzata come un pannello contenitore
  con drag and drop basta che erediti da rdfs:Container e che nel pannello dei servizi venga scelto un editor di layout come editor preferito

- il pannello dei servizi consente la scelta del tipo di servizio (view, edit, altro...)
  - dopo mostra il servizio prescelto con accanto l'indicazione se il servizio e' di istanza o se e' stato scelto autoaticamente
  in base alle preferenze per quel tipo di servizio espresse per il tipo o per un super tipo.
  - il servizio e' mostrato in una combo che se cliccata consente di cambiare il servizio scelto per l'istanza tra quelli compatibili o di cercarlo in un market.
=======
Here is the code for a <a href="https://github.com/alebellu/artusi-ssoup">JQuery and JSON-LD based implementation</a> of <a href="https://github.com/alebellu/ssoup">SSOUP</a>.

This project contains the js libraries used by the projects that are part of Artusi SSOUP.
>>>>>>> gh-pages
